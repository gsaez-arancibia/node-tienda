const { response, request } = require('express');
const { Product, Category } = require('../database');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const getProducts = async(req = request, res = response) => {
    const { query, category: categoryId, order = 'ASC' } = req.query;
    const whereProductStatement = {};
    const whereCategoryStatement = {};

    if (query) {
        whereProductStatement.name = { [Op.like]: `%${ query }%` };
    }

    if (categoryId && categoryId !== 0) {
        whereCategoryStatement.id = categoryId;
    }

    try {
        // Obtener productos agrupados por categoría
        const categories = await Category.findAll({
            where: whereCategoryStatement,
            order: [
                ['name', 'ASC'],
                [Product, 'price', order.toUpperCase()]
            ],
            include: [{
                model: Product,
                required: true,
                where: whereProductStatement,
                attributes: {
                    exclude: 'category'
                }
            }]
        });

        return res.json({ categories });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ err });
    }
}

const getProductById = async(req = request, res = response) => {
    try {
        const { id } = req.params;
        const product = await Product.findByPk(id);

        if (!product) {
            return res.status(404).json({ message: 'El producto no existe' });
        }

        return res.json({ product });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ err });
    }
}

module.exports = {
    getProducts,
    getProductById
}