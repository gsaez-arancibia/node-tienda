const { response, request } = require('express');
const { Category } = require('../database');

const getCategories = async(req = request, res = response) => {
    try {
        const categories = await Category.findAll({
            order: [
                ['name', 'ASC']
            ]
        });

        return res.json({ categories });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ err });
    }
}

module.exports = {
    getCategories
}