# Node - Tienda

Este repositorio contiene el código desarrollado para una aplicación web que simula una tienda online. Está desarrollado en el back-end con Node.js y en el front-end con Vanilla JavaScript.
Para realizar la instalación de forma local, seguir las instrucciones que se listan a continuación: 

### Pre-requisitos 📋

* Node.js - npm

### Instalación 🔧

_Con una terminal situarse dentro del directorio raíz donde fue clonado este repositorio, por ej: `~/git/node-tienda/` y ejecutar el siguiente comando_

* npm install

### Credenciales de Base de Datos y Variables de Ambiente de la Web App ⚙️

Copiar el archivo de ejemplo de configuración web de la App, en un nuevo archivo con nombre `.env`.

* Editar el archivo `.env` agregando las credenciales correspondientes, port, db_name, db_user, db_password y db_host.

## Ejecución ✒️

_Ejecutar el siguiente comando para correr el proyecto_

* node app

## Hosting 📦

Puedes ver de manera online el resultado de esta Web App en el siguiente [enlace](https://tienda-test-gsaez.herokuapp.com/).

## Construido con 🛠️

* [Node.js](https://nodejs.org/es/)
* [Notificaciones con Toastr](https://github.com/CodeSeven/toastr)
* [Iconos con Font Awesome](https://fontawesome.com/)

## Docs 📖

Puedes encontrar más documentación de este proyecto en [DOCS](docs/DOCS.md).