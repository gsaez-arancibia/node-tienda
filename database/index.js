const { Sequelize, DataTypes } = require('sequelize');
const db = {};

db.connection = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: 'mysql',
    define: {
        timestamps: false
    }
});

// Vincular modelos
db.Product = require('../models/product')(db.connection, DataTypes);
db.Category = require('../models/category')(db.connection, DataTypes);

// Asociar los modelos
db.Product.associate(db);
db.Category.associate(db);

module.exports = db;