# DOCS

## Ejercicio del Proyecto

Construir una tienda online que despliegue productos agrupados por la categoría a la que pertenecen, generando por separado back-end (API REST) y front-end (aplicación que la consuma) y utilizando la base de datos (MySQL) que está disponible para el desarrollo.
Se debe agregar un buscador, el cual tiene que estar implementado a nivel de servidor, mediante una API REST cuyo lenguaje y framework es de libre elección. Es decir, los datos de los productos deben llegar filtrados al cliente.
Opcionalmente, se puede implementar filtros por atributo, ordenar productos y paginación.
La aplicación de cliente tiene que estar desarrollada con Vanilla JavaScript pero si se puede usar librerías o componentes específicos como bootstrap, material, Jquery, entre otros.
Finalmente, disponibilizar la aplicación y el repositorio con el código en un hosting como por ejemplo Heroku, Netlity u otro. También se puede levantar el sitio en una cuenta AWS.

## Estructura de Directorios

Para comprender mejor como se organiza en general el código del proyecto, a continuación se detalla la estructura de directorios desde la raíz.

### Esquema general desde la raíz del proyecto
```
.
├── controllers         # Directorio de Controladores Web, manejan las peticiones de una ruta
│   ├── categories.js   # Controlador de las categorias
│   ├── products.js     # Controlador de los productos
├── database            # Directorio con las configuraciones de la base de datos
│   ├── index.js        # Instancia de conexión a la base de datos y vinculación de modelos
├── docs                # Directorio de Documentación
│   └── DOCS.md         # Esta documentación
├── models              # Directorio de modelos
│   ├── category.js     # Modelo de categoría
│   ├── product.js      # Modelo de producto
│   ├── server.js       # Modelo del rest server con Express
├── public              # Directorio Público, front-end del proyecto
│   ├── css             # Directorio que contiene los archivos CSS del front-end
│   ├── js              # Directorio que contiene los archivos JS del front-end
│   └── index.html      # Página principal de la Web App
├── routes              # Directorio que define las rutas/endpoints
│   ├── categories.js   # Archivo que contiene las rutas específicas para las categorias
│   ├── products.js     # Archivo que contiene las rutas específicas para los productos
├── app.js              # Archivo principal de la aplicación que sirve el servidor
├── example.env         # Archivo de ejemplo para configurar variables de entorno/ambiente, credenciales
├── package.json        # Dependencias/librerías js del proyecto
└── README.md           # Información general del proyecto, configuraciones e instrucciones
```

## Back-end (API REST)

El back-end del proyecto está construido con Node.js junto con Express que permite definir rutas que corresponden a métodos HTTP como son peticiones GET, POST, PUT, DELETE. A continuación, se listan los endpoints que se encuentran disponibles en esta Web App.

### Endpoints

| Método  | Url                | Query (Opcionales)               | Descripción                                            |
| ------- |:-------------------|:--------------------------------:|--------------------------------------------------------|
| GET     | /api/productos     | query, category, order(asc, desc)| Obtiene los productos agrupados por categoría          |
| GET     | /api/productos/:id | No posee                         | Obtiene un producto por su ID                          |
| GET     | /api/categorias    | No posee                         | Obtiene todas las categorías ordenadas alfabeticamente |

## Front-end

El front-end del proyecto está desarrollado con HTML5, CSS3 y Vanilla JavaScript y se utilizaron algunas librerías para mejorar la visualización/interacción con el usuario, como por ejemplo; Toastr, Google Fonts y Font Awesome. Este se encuentra ubicado en la carpeta 'public/' que será detallado a continuación:

### Detalle Directorio `public/`
```
public
│   ├── css                 # Directorio que contiene los archivos CSS del front-end
│   |   ├── normalize.css   # Estilos que hace que los navegadores representen todos los elementos de manera más consistente y en línea con los estándares modernos
│   |   ├── styles.css      # Estilos principales de la Web App
│   |   ├── toastr.min.css  # Estilos minificados de la librería Toastr
│   ├── js                  # Directorio que contiene los archivos JS del front-end
│   |   ├── cart.js         # Archivo que contiene la clase del carro de compras
│   |   ├── main.js         # Código JS principal de la Web App
│   |   ├── toastr.min.js   # Scripts minificados que de la librería Toastr
│   └── index.html          # Página principal de la Web App
```