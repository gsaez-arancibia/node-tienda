module.exports = (sequelize, DataTypes) => {
    const Category = sequelize.define('Category', {
        name: {
            type: DataTypes.STRING
        }
    }, {
        tableName: 'category'
    });

    Category.associate = function(models) {
        Category.hasMany(models.Product, { foreignKey: 'category' });
    }

    return Category;
}