module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define('Product', {
        name: {
            type: DataTypes.STRING
        },
        url_image: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.FLOAT
        },
        discount: {
            type: DataTypes.INTEGER
        },
        category: {
            type: DataTypes.INTEGER
        }
    }, {
        tableName: 'product'
    });

    Product.associate = function(models) {
        Product.belongsTo(models.Category, { foreignKey: 'category' });
    }

    return Product;
}