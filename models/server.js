const express = require('express');
const cors = require('cors');
const { connection } = require('../database');

class Server {

    constructor() {
        this.app = express();
        this.port = process.env.PORT;
        this.productsPath = '/api/productos';
        this.categoriesPath = '/api/categorias';

        // Base de datos
        this.dbConnection();

        // Middlewares
        this.middlewares();

        // Rutas
        this.routes();
    }

    async dbConnection() {
        try {
            await connection.authenticate();
            console.log('La conexión a la base de datos se ha establecido con éxito.');
        } catch (err) {
            throw new Error(err);
        }
    }

    middlewares() {
        // CORS
        this.app.use(cors());

        // Lectura y parseo del body
        this.app.use(express.json());

        // Directorio público
        this.app.use(express.static('public'));
    }

    routes() {
        this.app.use(this.productsPath, require('../routes/products'));
        this.app.use(this.categoriesPath, require('../routes/categories'));
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en el puerto', this.port);
        });
    }
}

module.exports = Server;