// Inicializar Carro
const cart = new Cart();
const countCart = document.querySelector('.cart-count');
countCart.innerText = cart._count;

document.addEventListener('DOMContentLoaded', async function() {
    // Referencias al DOM
    const form = document.querySelector('#form');
    const cartBtn = document.querySelector('#cart');
    const cartModal = document.querySelector('#modal');
    const categorySelect = document.getElementById('categories');
    const orderSelect = document.getElementById('order');
    const closeSpan = document.getElementsByClassName('close')[0];
    const clearCartBtn = document.querySelector('.btn-clear-cart');
    const payBtn = document.querySelector('.btn-pay');
    const logo = document.querySelector('.logo');

    // Obtener productos agrupados por categorías
    const result = await getProducts();

    if (result.length > 0) {
        createCategoriesSections(result);
    } else {
        createMainAlertInfo();
    }

    // Obtener categorías
    const categories = await getCategories();
    createCategoriesOptions(categories, categorySelect);

    // Submit Search
    form.addEventListener('submit', onSubmit);

    // Filtrar por categoría
    categorySelect.addEventListener('change', filterByCategory);

    // Filtrar por precio
    orderSelect.addEventListener('change', filterByPrice);

    // Abrir modal del carro
    cartBtn.addEventListener('click', () => {
        cartModal.style.display = 'block';

        if (cart._products.length > 0) {
            document.getElementById('info-clear').style.display = 'none';
            createCartTable();
        } else {
            document.getElementById('cart-table').style.display = 'none';
            document.getElementById('modal-footer').style.display = 'none';
        }
    });

    // Cerrar modal del carro
    closeSpan.addEventListener('click', () => cartModal.style.display = 'none');

    // Vaciar Carro
    clearCartBtn.addEventListener('click', clearCart);

    // Realizar Pago (?)
    payBtn.addEventListener('click', () => toastr.warning('Esta funcionalidad no se encuentra disponible 🙁.'));

    // Recargar Página
    logo.addEventListener('click', () => window.location = '/');
    
    // Setear opciones de Toastr 
    setToastrOptions();
});

// ---------------------------------- INICIO CALLS API ---------------------------------- //
const getProducts = async(query, category, order) => {
    const loading = document.querySelector('.loading');
    const mainContent = document.querySelector('.main-content');
    let url = '/api/productos?a=a';

    loading.style.display = '';
    mainContent.innerHTML = '';

    if (query) url += `&query=${ query }`;
    if (category) url += `&category=${ category }`;
    if (order) url += `&order=${ order }`;
    
    try {
        const res = await fetch(url);
        const { categories } = await res.json();
        return categories;
    } catch (err) {
        console.log(err);
        return [];
    } finally {
        loading.style.display = 'none';
    }
}

const getCategories = async() => {
    const url = '/api/categorias';

    try {
        const res = await fetch(url);
        const { categories } = await res.json();
        return categories;
    } catch (err) {
        console.log(err);
        return [];
    }
}

const getProductById = async(id) => {
    const url = `/api/productos/${ id }`;

    try {
        const res = await fetch(url);
        const { product } = await res.json();
        return product;
    } catch (err) {
        console.log(err);
        return null;
    }
}
// ---------------------------------- FIN CALLS API ---------------------------------- //

// ---------------------------------- INICIO DOM ---------------------------------- //
const createCategoriesSections = (categories = []) => {
    const mainContent = document.querySelector('.main-content');

    for (category of categories) {
        const categorySection = document.createElement('section');
        const categoryTitle = document.createElement('h2');
        categoryTitle.innerText = `${ category.name.toUpperCase() }`;
        categorySection.append(categoryTitle);

        if (category.Products.length > 0) {
            const productsDiv = document.createElement('div');
            productsDiv.className = 'products';
            
            // Crear tarjeta para cada producto
            category.Products.forEach(product => {
                createProductCard(productsDiv, product);
            });

            categorySection.append(productsDiv);
        } else {
            const alertDiv = document.createElement('div');
            alertDiv.classList.add('alert', 'alert-info');
            alertDiv.innerText = 'No hay productos disponibles para esta categoría.';

            categorySection.append(alertDiv);
        }

        mainContent.append(categorySection);
    }
}

const createProductCard = (productsDiv, product) => {
    const card = document.createElement('div');
    const cardHeader = document.createElement('div');
    const productImg = document.createElement('img');
    const cardBody = document.createElement('div');
    const cardTitle = document.createElement('h3');
    const cardFooter = document.createElement('div');
    const productDetail = document.createElement('div');
    const realPrice = document.createElement('p');
    const discountPrice = document.createElement('p');
    const btnCartDiv = document.createElement('div');
    btnCartDiv.innerHTML = `
        <button type="button" class="btn-add-product" data-id="${ product.id }" onclick="addProductToCart(this)">
            Agregar al Carro
            <i class="fa-solid fa-cart-arrow-down"></i>
        </button>
    `;

    card.className = 'card';
    cardHeader.className = 'card-header';
    productImg.className = 'product-image';
    productImg.src = product.url_image 
        ? product.url_image 
        : 'https://www.giulianisgrupo.com/wp-content/uploads/2018/05/nodisponible.png';
    productImg.alt = `${ product.name }`;
    cardBody.className = 'card-body';
    cardTitle.className = 'card-title';
    cardTitle.innerText = `${ product.name }`;
    cardFooter.className = 'card-footer';
    productDetail.className = 'product-detail';
    realPrice.className = 'real-price';
    realPrice.innerText = `${ formatToCLP(product.price) }`;
    discountPrice.className = 'discount-price';

    cardHeader.append(productImg);

    if (product.discount !== 0) {
        const discount = document.createElement('p');
        discount.className = 'discount';
        discount.innerText = `-${ product.discount }%`;
        cardHeader.append(discount);

        realPrice.classList.add('line-through');
        const valueDiscountPrice = calculateDiscountedPrice(product.price, product.discount);
        discountPrice.innerText = `${ formatToCLP(valueDiscountPrice) }`;
        discountPrice.classList.add('fw-bold');
    } else {
        realPrice.classList.add('fw-bold');
    }

    cardBody.append(cardTitle);
    cardFooter.append(productDetail);
    cardFooter.append(btnCartDiv);
    productDetail.append(realPrice);
    productDetail.append(discountPrice);

    card.append(cardHeader);
    card.append(cardBody);
    card.append(cardFooter);
    productsDiv.append(card);
}

const createCategoriesOptions = (categories, categorySelect) => {
    categories.forEach(category => {
        const { id, name } = category;
        categorySelect.options[categorySelect.options.length] = new Option(`${ name.toUpperCase() }`, `${ id }`);
    });
}

const createCartTable = () => {
    const cartTable = document.querySelector('#cart-table');
    cartTable.style.display = '';
    document.getElementById('modal-footer').style.display = '';

    const total = document.querySelector('.total');
    let tbody = cartTable.getElementsByTagName('tbody')[0];
    let tr = '';

    if (!tbody) {
        tbody = document.createElement('tbody');
    }

    cart._products.forEach(product => {
        const image = product.url_image 
            ? product.url_image 
            : 'https://www.giulianisgrupo.com/wp-content/uploads/2018/05/nodisponible.png';

        tr += `
            <tr data-id="${ product.id }">
                <td>
                    <img class="product-image" src="${ image }" alt="${ product.name }">
                </td>
                <td>${ product.name }</td>
                <td>
                    <div class="product-quantity">
                        <button type="button" class="btn-increase" onclick="increaseQuantity(this)">
                            <i class="fa-solid fa-arrow-up"></i>
                        </button>
                        <span>${ product.quantity }</span>
                        <button type="button" class="btn-decrease" onclick="decreaseQuantity(this)">
                            <i class="fa-solid fa-arrow-down"></i>
                        </button>
                    </div>  
                </td>
                <td>${ formatToCLP(product.price) }</td>
                <td>
                    <button class="btn-delete-product" onclick="deleteProductToCart(this)">
                        <i class="fa-solid fa-circle-xmark"></i>
                    </button>
                </td>
            </tr>
        `;
    });

    tbody.innerHTML = tr;
    cartTable.append(tbody);
    total.innerText = `Total: ${ formatToCLP(cart._total) }`;
}

const showModalAlertInfo = () => {
    document.getElementById('cart-table').style.display = 'none';
    document.getElementById('modal-footer').style.display = 'none';
    document.getElementById('info-clear').style.display = 'block';
}

const createMainAlertInfo = () => {
    const alertDiv = document.createElement('div');
    alertDiv.classList.add('alert', 'alert-info');
    alertDiv.innerText = 'No se encontraron productos.';

    const mainContent = document.querySelector('.main-content');
    mainContent.innerHTML = '';
    mainContent.append(alertDiv);
}
// ---------------------------------- FIN DOM ---------------------------------- //

// ---------------------------------- EVENTS HANDLERS ---------------------------------- //
const onSubmit = async(e) => {
    e.preventDefault();
    const query = e.target[0].value.trim();
    const category = document.getElementById('categories');
    const order = document.querySelector("select[name='order']").value;
    category.value = 0;

    const result = await getProducts(query, null, order);

    if (result.length > 0) {
        createCategoriesSections(result);
    } else {
        createMainAlertInfo();
    }
}

const filterByCategory = async(e) => {
    const id = Number(e.target.value);
    const query = document.querySelector("input[name='query']");
    const order = document.querySelector("select[name='order']").value;
    query.value = '';

    const result = await getProducts(null, id, order);

    if (result.length > 0) {
        createCategoriesSections(result);
    } else {
        createMainAlertInfo();
    }
}

const filterByPrice = async(e) => {
    const order = e.target.value;
    const query = document.querySelector("input[name='query']");
    const categoryId = Number(document.querySelector("select[name='category']").value);
    query.value = '';
    
    const result = await getProducts(null, categoryId, order);

    if (result.length > 0) {
        createCategoriesSections(result);
    } else {
        createMainAlertInfo();
    }
}

const addProductToCart = async(e) => {
    const id = Number(e.getAttribute('data-id'));
    const product = await getProductById(id);

    if (!product) {
        toastr.error(`Ha ocurrido un error al intentar agregar el producto al carro.`);
        return;
    }

    if (product.discount !== 0) {
        product.price = calculateDiscountedPrice(product.price, product.discount);
    }

    cart.addProduct(product);
    countCart.innerText = cart._count;
    toastr.success(`${ product.name } se ha agregado al carro.`);
}

const increaseQuantity = (e) => {
    const row = e.parentElement.parentElement.parentElement;
    const id = Number(row.getAttribute('data-id'));
    const value = cart.increaseProduct(id);

    if (!value) {
        toastr.error('Ha ocurrido un error al intentar aumentar la cantidad del producto.');
        return;
    }

    const quantity = row.getElementsByTagName('span')[0];
    const total = document.querySelector('.total');
    quantity.innerText = value;
    total.innerText = `Total: ${ formatToCLP(cart._total) }`;
    countCart.innerText = cart._count;
}

const decreaseQuantity = (e) => {
    const row = e.parentElement.parentElement.parentElement;
    const id = Number(row.getAttribute('data-id'));
    const value = cart.decreaseProduct(id);

    if (value === -1) {
        toastr.error('Ha ocurrido un error al intentar disminuir la cantidad del producto.');
        return;
    }

    const quantity = row.getElementsByTagName('span')[0];
    const total = document.querySelector('.total');

    if (value > 0) {
        quantity.innerText = value;
        total.innerText = `Total: ${ formatToCLP(cart._total) }`;
    } else {
        const tbody = document.querySelector('#cart-table').getElementsByTagName('tbody')[0];
        tbody.removeChild(row);
        total.innerText = `Total: ${ formatToCLP(cart._total) }`;
        toastr.success('El producto ha sido eliminado del carro.');

        if (tbody.rows.length === 0) {
            showModalAlertInfo();
        }
    }

    countCart.innerText = cart._count;
}

const deleteProductToCart = (e) => {
    const row = e.parentElement.parentElement;
    const id = Number(row.getAttribute('data-id'));
    const isDeleted = cart.deleteProduct(id);

    if (!isDeleted) {
        toastr.error('Ha ocurrido un error al intentar eliminar el producto del carro.');
        return;
    }

    const tbody = document.querySelector('#cart-table').getElementsByTagName('tbody')[0];
    const total = document.querySelector('.total');
    tbody.removeChild(row);
    total.innerText = `Total: ${ formatToCLP(cart._total) }`;
    
    if (tbody.rows.length === 0) {
        showModalAlertInfo();
    }

    countCart.innerText = cart._count;
    toastr.success('El producto ha sido eliminado del carro.');
}

const clearCart = () => {
    cart.clear();
    countCart.innerText = cart._count;

    showModalAlertInfo();
    toastr.success('El carro se ha vaciado correctamente.');
}
// ---------------------------------- FIN EVENTS HANDLERS ---------------------------------- //

// ---------------------------------- INICIO UTILS ---------------------------------- //
const calculateDiscountedPrice = (price, discount) => {
    const value = Math.round((price * ((100 - discount) / 100)));
    
    return Math.round(value / 10) * 10;
}

const formatToCLP = (price) => {
    return new Intl.NumberFormat('es-CL', {currency: 'CLP', style: 'currency'}).format(price);
}

const setToastrOptions = () => {
    toastr.options = {
        closeButton: false,
        debug: false,
        newestOnTop: false,
        progressBar: false,
        positionClass: 'toast-bottom-right',
        preventDuplicates: false,
        onclick: null,
        showDuration: '300',
        hideDuration: '1000',
        timeOut: '2000',
        extendedTimeOut: '1000',
        showEasing: 'swing',
        hideEasing: 'linear',
        showMethod: 'fadeIn',
        hideMethod: 'fadeOut'
    }
}
// ---------------------------------- FIN UTILS ---------------------------------- //