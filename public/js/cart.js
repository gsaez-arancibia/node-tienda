class Cart {
    _products = [];

    constructor() {
        this._products = JSON.parse(localStorage.getItem('cart')) || [];
    }

    get _total() {
        return this._products.reduce((sum, product) => sum + (product.price * product.quantity), 0);
    }

    get _count() {
        return this._products.reduce((sum, product) => sum + product.quantity, 0);
    }

    addProduct({ id, url_image, name, price, quantity = 1 }) {
        const product = { id, url_image, name, price, quantity };
        const idx = this._products.findIndex(product => product.id === id);

        if (idx !== -1) {
            this._products[idx].quantity++;
        } else {
            this._products.push(product);
        }

        this.save();
    }

    increaseProduct(id) {
        const idx = this._products.findIndex(product => product.id === id);

        if (idx === -1) {
            return null;
        }

        this._products[idx].quantity++;
        this.save();

        return this._products[idx].quantity;
    }

    decreaseProduct(id) {
        const idx = this._products.findIndex(product => product.id === id);

        if (idx === -1) {
            return -1;
        }

        const quantity = --this._products[idx].quantity;

        if (quantity === 0) {
            this._products.splice(idx, 1);
        }

        this.save();

        return quantity;
    }

    deleteProduct(id) {
        const idx = this._products.findIndex(product => product.id === id);

        if (idx === -1) {
            return false;
        }

        this._products.splice(idx, 1);
        this.save();
        
        return true;
    }

    clear() {
        this._products = [];
        localStorage.removeItem('cart');
    }

    save() {
        localStorage.setItem('cart', JSON.stringify(this._products));
    }
}